/******************************************************************************
 * Данная утилита служит для генерации файла прошивок ПЛИС по заданному образцу.
 * Вызывается как:
 * lxfirm_prepare: <файл прошивки> <файл образца> <результирующий файл>
 *
 * Результирующий файл является файлом образца, в котором символ @DATA@ заменен
 * на содержимое файла прошивки, закодированном в Radix64 для текстового
 * представления.
 * Также если в файле образца встречается @CRC32@, то он заменяется на
 * значение CRC32 исходного файла прошивки (в формате 0x%08X)
 * ****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "radix64.h"
#include "fast_crc.h"

typedef enum {
    PLACEHOLDER_TYPE_DATA = 1,
    PLACEHOLDER_TYPE_CRC32 = 2
} t_placeholder_types;

#define PLACEHOLDER_START_SYMBOL  '@'

#define PLACEHOLDER_MAX_LEN   32


#define CRC32_PUT_SIZE  10

typedef struct {
    t_placeholder_types type;
    const char *str;
    size_t size;
} t_placeholder;

static const t_placeholder f_placeholders[] = {
    {PLACEHOLDER_TYPE_DATA,  "@DATA@",  sizeof("@DATA@")-1},
    {PLACEHOLDER_TYPE_CRC32, "@CRC32@", sizeof("@CRC32@")-1},
    {0, NULL, 0}
};


static const t_placeholder *f_check_placeholder(const char* buf, unsigned rem_size) {
    const t_placeholder *fnd_holder = NULL;
    if (buf[0] == PLACEHOLDER_START_SYMBOL) {
        const t_placeholder *cur_holder;
        for (cur_holder = &f_placeholders[0];
             (cur_holder->str!=NULL) && (fnd_holder == NULL); cur_holder++) {
            if ((cur_holder->size <= rem_size)  && !memcmp(buf, cur_holder->str, cur_holder->size))
                fnd_holder = cur_holder;
        }
    }

    return fnd_holder;
}



#define READ_SIZE 4096

static int f_file_wr_exact(FILE* f, char* buf, size_t size) {
    size_t wr_size;
    if (size==0)
        return 0;

    wr_size = fwrite(buf, 1, size, f);
    if (wr_size!=size) {
        fprintf(stderr, "File write error..\n");
        return -5;
    }
    return 0;
}

static int f_write_data(FILE* in, FILE* out, uint32_t *pcrc){
    static unsigned char in_buf[3*256];
    static char out_buf[4*256];
    int err = 0;
    uint32_t crc32 = 0;


    do {
        size_t rd_size = fread(in_buf, 1, sizeof(in_buf), in);
        if ((rd_size != sizeof(in_buf)) && (ferror(in))) {
            fprintf(stderr, "File read error..\n");
            err = -4;
        } else if (rd_size) {
            size_t size;
            crc32 = CRC32_Block8(crc32, in_buf, rd_size);

            size = Radix64_Encode(in_buf, rd_size, out_buf);
            err = f_file_wr_exact(out, out_buf, size);
        }
    } while (!err && !feof(in));


    if (!err && (pcrc!=NULL)) {
        *pcrc = crc32;
    }
    return err;
}

static int f_wr_crc32(FILE *f, uint32_t crc32) {
    char crc_str[CRC32_PUT_SIZE+1];
    sprintf(crc_str, "0x%08X", crc32);
    return f_file_wr_exact(f, crc_str, CRC32_PUT_SIZE);
}

int main(int argc, char **argv) {
    static char buf[READ_SIZE+PLACEHOLDER_MAX_LEN];

    int err = 0;
    FILE *info_file=NULL, *firm_file = NULL, *out_file = NULL;
    if (argc < 4) {
        fprintf(stderr, "Insufficient argument. Usage: lxfirm_prepare <firmware_file> <template_file> <result_file>\n");
        return -1;
    }


    firm_file = fopen(argv[1], "rb");
    if (firm_file==NULL) {
        fprintf(stderr, "Cannot open firmware file (%s)!\n", argv[1]);
        err = -2;
    }

    if (!err) {
        info_file = fopen(argv[2], "r");
        if (info_file==NULL) {
            fprintf(stderr, "Cannot open info file (%s)!\n", argv[2]);
            err = -2;
        }
    }

    if (!err) {
        out_file = fopen(argv[3], "w");
        if (out_file==NULL) {
            fprintf(stderr, "Cannot create output file (%s)!\n", argv[3]);
            err = -2;
        }
    }


    if (!err) {
        size_t proc_size=0, rd_size;
        int crc_put_pos = -1;
        uint32_t crc=0;

        do {
            rd_size = fread(&buf[proc_size], 1, sizeof(buf)-proc_size, info_file);
            if (ferror(info_file)) {
                fprintf(stderr, "File read error!!\n");
                err = -3;
            } else {
                size_t i;
                size_t wr_pos = 0;

                proc_size = rd_size= rd_size+proc_size;
                if (proc_size > READ_SIZE)
                    proc_size = READ_SIZE;

                for (i=0; i < proc_size; i++) {
                    const t_placeholder *placeholder = f_check_placeholder(&buf[i], rd_size - i);
                    if (placeholder != NULL) {
                        size_t wr_size= (i-wr_pos);
                        err = f_file_wr_exact(out_file, &buf[wr_pos], wr_size);

                        if (placeholder->type == PLACEHOLDER_TYPE_DATA) {
                            err = f_write_data(firm_file, out_file, &crc);
                        } else if (placeholder->type == PLACEHOLDER_TYPE_CRC32) {
                            crc_put_pos = ftell(out_file);
                            err = f_wr_crc32(out_file, 0);
                        }

                        if (!err) {
                            i+=placeholder->size-1;
                            wr_pos = i+1;
                        }
                    }
                }

                if (!err && (wr_pos != proc_size)) {
                    err = f_file_wr_exact(out_file, &buf[wr_pos], proc_size-wr_pos);
                }

                if (!err) {
                    if (proc_size!=rd_size) {
                        memmove(buf, &buf[proc_size], rd_size-proc_size);
                    }
                    proc_size = rd_size - proc_size;
                }
            }
        } while (!err && !feof(info_file));

        if (!err){
            err = f_file_wr_exact(out_file, buf, proc_size);
        }


        if (!err && (crc_put_pos>=0)) {
            fseek(out_file, crc_put_pos, SEEK_SET);
            err = f_wr_crc32(out_file, crc);
        }
    }

    if (firm_file)
        fclose(firm_file);
    if (info_file)
        fclose(info_file);
    if (out_file)
        fclose(out_file);


    return err;
}
